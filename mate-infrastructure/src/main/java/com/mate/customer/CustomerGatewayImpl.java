package com.mate.customer;

import com.mate.domain.customer.Customer;
import com.mate.domain.customer.gateway.CustomerGateway;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class CustomerGatewayImpl implements CustomerGateway {

    @Autowired
    private CustomerMapper customerMapper;

    public Customer getByById(String customerId){
        CustomerDO customerDO = customerMapper.getById(customerId);
        Customer customer = new Customer();
        BeanUtils.copyProperties(customerDO, customer);
      return customer;
    }
}
