package com.mate.domain.customer.gateway;

import com.mate.domain.customer.Customer;

public interface CustomerGateway {
    public Customer getByById(String customerId);
}
