package com.mate.domain.customer.gateway;

import com.mate.domain.customer.Customer;
import com.mate.domain.customer.Credit;

//Assume that the credit info is in antoher distributed Service
public interface CreditGateway {
    public Credit getCredit(String customerId);
}
